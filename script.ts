class Person {
  Name: string;
  Age: number;
  Salary: number;
  Sex: string;
  constructor(Name: string, Age: number, Salary: number, Sex: string) {
    this.Name = Name;
    this.Age = Age;
    this.Salary = Salary;
    this.Sex = Sex;
  }

  static Sort(array: any, field: string, sortType: string): object[] {
    if (array.length <= 1) {
      return array;
    }
    
    let pivot: number | string = array[0][field];

    let leftArr: object[] = [];
    let rightArr: object[] = [];

    for (let i = 1; i < array.length; i++) {
      if (sortType === "asc") {
        array[i][field] < pivot
          ? leftArr.push(array[i])
          : rightArr.push(array[i]);
      } else if (sortType === "desc") {
        array[i][field] > pivot
          ? leftArr.push(array[i])
          : rightArr.push(array[i]);
      }
    }

    if (leftArr.length > 0 && rightArr.length > 0) {
      return [
        ...this.Sort(leftArr, field, sortType),
        array[0],
        ...this.Sort(rightArr, field, sortType),
      ];
    } else if (leftArr.length > 0) {
      return [...this.Sort(leftArr, field, sortType), array[0]];
    } else {
      return [array[0], ...this.Sort(rightArr, field, sortType)];
    }
  }
}

let p1 = new Person("simran", 22, 10000, "male");
let p2 = new Person("rohan", 32, 11000, "male");
let p3 = new Person("preeti", 21, 10500, "female");
let p4 = new Person("rahul", 53, 10550, "male");
let p5 = new Person("raj", 39, 9000, "male");
let p6 = new Person("kirti", 19, 500, "female");
let p7 = new Person("karan", 55, 2500, "male");

let personsArr: object[] = [p1, p2, p3, p4, p5, p6, p7];
console.log(personsArr);

// sort on the basis of salary in ascending order
console.log(personsArr);
console.log(Person.Sort(personsArr, "Salary", "asc"));
console.log(personsArr);

// sort on the basis on age in descending order
console.log(personsArr);
console.log(Person.Sort(personsArr, "age", "desc"));
console.log(personsArr);
