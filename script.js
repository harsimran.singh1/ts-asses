var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
var Person = /** @class */ (function () {
    function Person(Name, Age, Salary, Sex) {
        this.Name = Name;
        this.Age = Age;
        this.Salary = Salary;
        this.Sex = Sex;
    }
    Person.Sort = function (array, field, sortType) {
        if (array.length <= 1) {
            return array;
        }
        var pivot = array[0][field];
        var leftArr = [];
        var rightArr = [];
        for (var i = 1; i < array.length; i++) {
            if (sortType === "asc") {
                array[i][field] < pivot
                    ? leftArr.push(array[i])
                    : rightArr.push(array[i]);
            }
            else if (sortType === "desc") {
                array[i][field] > pivot
                    ? leftArr.push(array[i])
                    : rightArr.push(array[i]);
            }
        }
        if (leftArr.length > 0 && rightArr.length > 0) {
            return __spreadArray(__spreadArray(__spreadArray([], this.Sort(leftArr, field, sortType), true), [
                array[0]
            ], false), this.Sort(rightArr, field, sortType), true);
        }
        else if (leftArr.length > 0) {
            return __spreadArray(__spreadArray([], this.Sort(leftArr, field, sortType), true), [array[0]], false);
        }
        else {
            return __spreadArray([array[0]], this.Sort(rightArr, field, sortType), true);
        }
    };
    return Person;
}());
var p1 = new Person("simran", 22, 10000, "male");
var p2 = new Person("rohan", 32, 11000, "male");
var p3 = new Person("preeti", 21, 10500, "female");
var p4 = new Person("rahul", 53, 10550, "male");
var p5 = new Person("raj", 39, 9000, "male");
var p6 = new Person("kirti", 19, 500, "female");
var p7 = new Person("karan", 55, 2500, "male");
var personsArr = [p1, p2, p3, p4, p5, p6, p7];
console.log(personsArr);
// sort on the basis of salary in ascending order
console.log(personsArr);
console.log(Person.Sort(personsArr, "Salary", "asc"));
console.log(personsArr);
// sort on the basis on age in descending order
console.log(personsArr);
console.log(Person.Sort(personsArr, "age", "desc"));
console.log(personsArr);
